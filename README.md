# Wrist Watch Recognition via Convolutional Neural Network

The identification of some unknown watch (worn by a celebrity, e.g.) is a common request within the watch 
community and watch-related forums.
As an explorative exercise for a potential large-scale project, 
I scraped [WatchRecon](https://www.watchrecon.com/) for images of two iconic watches: 
Omega Speedmaster and Rolex Submariner.
WatchRecon itself scrapes the web to collate "popular watch sales forums and builds a centralized search index".
With and without image augmentation, I attempt to train a [Convolutional Neural Network (CNN)](https://en.wikipedia.org/wiki/Convolutional_neural_network) 
for the binary classification of future images of these two watches.
The scope of this project could be generalized to "chronograph" and "diver", e.g., or 
broadened to include more styles of watches or more specific watch references.




1. [Scraping](#scrape)
2. [Loading](#load)
3. [Augmenting](#augment)
4. [CNN](#cnn)
5. [Plotting](#plot)
6. [Predicting](#predict)
7. [Other Datasets](#other)
8. [Future Expansions](#future)


## [Scraping](https://gitlab.com/Bonatt/WristWatchIdentification/blob/master/scrape.py) <a name="scrape"></a>

I briefly learned how to use the HTML parser [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/) about a year ago, 
and I figured I could now employ it here for some real-world scraping.
I had originally intended to scrape from [Google images](https://images.google.com/) but soon
realized it was [not easy and possibly not legal](https://stackoverflow.com/questions/36438261/extracting-images-from-google-images-using-src-and-beautifulsoup).
Thus I had to think of other sources for numerous and varied images of Omega Speedmasters and Rolex Submariners
(herein I will now be referring to them as speedy and sub, respectively).
Then I remembered WatchRecon, which I browse often enough.

WatchRecon scrapes from watch sales forums and neatly collates submissions with filter and search functionality.
A page of thumbnails with forum, price, time, etc. are displayed in a grid, with 48 items per page.
It is these 186x140-sized thumbnails that I scrape. 
Note that I have received explicit permission from WatchRecon to do this
with the stipulation that I "throttle the fetches to at most 1 fetch per second".

```python
# Specify the url architecture
# e.g., https://www.watchrecon.com/?query=omega+speedmaster&current_page=1
watchrecon = 'https://www.watchrecon.com/'
query = '?query='
currentpage = '&current_page='

# Watches to scrape
# [watch/directory, query string]
data = './data/'
watches = [ ['speedy', 'omega+speedmaster'],
            ['sub', 'rolex+submariner'] ]

# Number of current_pages to scrape.
# WatchRecon pages are time-dependent -- newer first, older last.
firstpage = 1
maxpage = 5
pages = range(firstpage, maxpage+1)
```

The HTML is retrieved and put into a BeautifulSoup format. The image URLs
are parsed from this and downloaded to a ```data``` directory.
Only about 30-40 of each 48-image page are useable. I manually scan the
downloaded images and delete them to Trash if the images are not of the watch itself,
are taken too far away, etc. To elimiate redundancy, if the image id already exists in data or Trash,
the image is not downloaded. To appease WatchRecon Support, after each image download 
there exists a pause of three seconds.

Because WatchRecon is in reverse chronological order with some lifetime, only relatively-recent
images can be scraped. And once I've initially scraped them, new images may be scraped at
the rate that sales are submitted.
Now that some number of images are downloaded they can be utilized by the rest of the scripts...


## [Loading](https://gitlab.com/Bonatt/WristWatchIdentification/blob/master/load.py) <a name="load"></a>
Below are two collages of a subset of speedy and sub images, respectively.
![speedy](./plots/speedyfamily_n=169.png)
![sub](./plots/subfamily_n=144.png)

Images are downloaded at a size of 186x140 pixels. Because these images are all the same size
I don't necessarily need to resize them. I do anyway: to a square 128x128. This size just seemed reasonable.

Images are saved to memory via [OpenCV](https://opencv.org/). If augmenting the images is desired,
the images may be augmented as described in the following section. 
All images are shuffled and returned as two datasets: training and validation.
Data is currently in short supply therefore scraped images are currently not employed for testing.


## [Augmenting](https://gitlab.com/Bonatt/WristWatchIdentification/blob/master/augment.py) <a name="augmenting"></a>
Augmentation of images is a method to artificially increase statistics and model robustness.
With only moderate changes and the inclusion of a necessary wrapper, I copied the functions as created by Prasad Pai, 
found [here](https://medium.com/ymedialabs-innovation/data-augmentation-techniques-in-cnn-using-tensorflow-371ae43d5be9).
From these, I've employed a smaller selection of augmentations: scaling, translating, rotating, flipping, and gaussian noise.
These are applied inline and are not saved as distinct images.
With all of these augmentations active, 271 images expands to 4336 images:

```
If N = instances in raw data, and M = instances in augmented data
M = N * (1 + nscalings + ntranslations + nrotations + nflips + ngaussians)
```
```
M = 271 * (1 + 4 + 4 + 3 + 3 + 1) = 4336
```

Augmentation increases training time by _a lot_: from ~10 minutes to ~2 hours. 
Additionally, augmenting so few images still leads to rapid overfitting.
For most trialing so far, augmentations have not been added to the datasets.


## [CNN](https://gitlab.com/Bonatt/WristWatchIdentification/blob/master/cnn.py) <a name="cnn"></a>

For this CNN implementation in Tensorflow, I followed the [tutorial](http://cv-tricks.com/tensorflow-tutorial/training-convolutional-neural-network-for-image-classification/) by Ankit Sachan of CV-Tricks.
Ankit's example classified cats and dogs but was easily transported to my own project. 
He also provides a script that loads training images and predicts test images 
but I had already created my own before finding his tutorial. 
I have expanded his work in an attempt to reduce redundancy, increase readability, and improve transferability.
TensorFlow covention may have been lost, however.

Notes: I've recently learned of employing FFT in place of filtering 
[[1](https://en.wikipedia.org/wiki/Convolution_theorem), 
[2](https://arxiv.org/abs/1412.7580), 
[3](https://cgi.csc.liv.ac.uk/~frans/PostScriptFiles/ecmlPratt_2017.pdf)]
but I will not explore this here.
I may eventually aim to include a localization algorithm to better track watches in training images 
(many training images are of a watch amongst boxes or even amongst other watches; as of now
these are pruned or, if not too extreme, retained for generality. 
Augmentation would help here, too).

The convolutional neural network has the following architecture:

```
Architecture:
input shape: (?, 128, 128, 3)
 conv layer shape: (?, 128, 128, 32)
 output max pooling layer shape: (?, 64, 64, 32)
input shape: (?, 64, 64, 32)
 conv layer shape: (?, 64, 64, 32)
 output max pooling layer shape: (?, 32, 32, 32)
input shape: (?, 32, 32, 32)
 conv layer shape: (?, 32, 32, 64)
 output max pooling layer shape: (?, 16, 16, 64)
input shape: (?, 16, 16, 64)
 conv layer shape: (?, 16, 16, 64)
 output max pooling layer shape: (?, 8, 8, 64)
input shape: (?, 8, 8, 64)
 conv layer shape: (?, 8, 8, 128)
 output max pooling layer shape: (?, 4, 4, 128)
input shape: (?, 4, 4, 128)
 output flat layer shape: (?, 2048)
input shape: (?, 2048)
 output fc layer shape: (?, 128)
 dropout: 0.5
input shape: (?, 128)
 output fc layer shape: (?, 2)
```

This architecture is fairly conventional in that deeper layers observe larger
structures. See [this document](https://kaggle2.blob.core.windows.net/forum-message-attachments/69182/2287/A%20practical%20theory%20for%20designing%20very%20deep%20convolutional%20neural%20networks.pdf)
for some tips. One of my main concerns with modern machine learning is the
free-form nature of neural network architecture design. I'd like nothing more than to
learn or discover analytical rules for such implementation. Note that I did not
follow the above document when designing my architecure, though there is no
doubt that a redux will eventually be necessary.


Currently, three forms of [regularization](https://en.wikipedia.org/wiki/Regularization_(mathematics)) are employed:

1. [Batch normalization](https://arxiv.org/abs/1502.03167) is applied to all 
convolutional (conv) layers and to the first fully connected (fc) layer.
2. A [dropout](https://en.wikipedia.org/wiki/Dropout_(neural_networks)) of 0.5 is 
also applied between the first fully connected layer. These
are applied during training but not during validation or testing.
3. An early stop is enforced when validation loss begins to increase, akin to:

```python
patience = 10 # epochs
if valid_loss[-1] > valid_loss[-patience:-1]: break
```

The following hyperdata and data are recorded for post-training analysis:
```
Augments:
Training images:   363
Validation images: 91
Model: speedy-sub-model9
[Architecture: ...]
Epochs, max: 50
Epochs, early stop: 42
Batchsize: 32
Batches: 12
Learning rate: 0.0001
Dropout: 0.5
Batch normalization: True
Training time: 2365 sec = 39.42 min = 0.657 hr
```
```
Training Accuracy, Validation Accuracy, Training Loss, Validation Loss
```
Currently the only parameters logged for efficacy and performance are
training accuracy and loss, and validation accuracy and loss.
Accuracy and loss are saved at the end of each epoch. 
Loss here is [cross entropy](https://en.wikipedia.org/wiki/Cross_entropy).


## [Plotting](https://gitlab.com/Bonatt/WristWatchIdentification/blob/master/plot.py) <a name="plot"></a>

__Below are two plots from speedy-sub-model9 (with hyperdata shown above):__

![acc](./plots/speedy-sub-model9_acc.png)

![loss](./plots/speedy-sub-model9_loss.png)

Training accuracy quickly converges to 100% but validation accuracy slowly converges to 90%.
Similarly, training loss quickly decreases to 0.03 but validation loss slowly decreases to below 0.3 before increasing.
An increased learning rate may provide better optimization.


__Below I've increased the learning rate from 1e-4 to 1e-3 for speedy-sub-model10:__
```
Augments:
Training images:   363
Validation images: 91
Model: speedy-sub-model10
[Architecture: ...]
Epochs, max: 50
Epochs, early stop: 15
Batchsize: 32
Batches: 12
Learning rate: 0.001
Dropout: 0.5
Batch normalization: True
Training time: 821 sec = 13.68 min = 0.228 hr
```

![acc](./plots/speedy-sub-model10_acc.png)

![loss](./plots/speedy-sub-model10_loss.png)

Wow! Training accuracy reached 100% in three epochs!
Validation accuracy didn't improve much past the previous 90%, though.
Both losses decreased quicker too. But minimum training loss is lower than previously,
while validation loss isn't much lower.
I'd say this learning rate is almost better in every way, especially when comparing 
a training time of ~14 epochs to ~40 epochs. Batch normalization
allows for an increased learning rate. That was my motivation here, and I think
this proves it.


__Going one step further, I've increased the learning again from 1e-3 to 1e-2 for speedy-sub-model11:__

```
Augments:
Training images:   363
Validation images: 91
Model: speedy-sub-model11
[Architecture: ...]
Epochs, max: 50
Epochs, early stop: 28
Batchsize: 32
Batches: 12
Learning rate: 0.01
Dropout: 0.5
Batch normalization: True
Training time: 1516 sec = 25.28 min = 0.421 hr
```

![acc](./plots/speedy-sub-model11_acc.png)

![loss](./plots/speedy-sub-model11_loss.png)

![loss](./plots/speedy-sub-model11_lossLog.png)

Wow again! A validation loss of almost 900! Though admittedly (gladly) it
does eventually decrease to around 0.5 which isn't too far off from the previous 
0.27. Validation accuracy settles around 90% as usual,
but training loss is again very low compared to validation loss. 
This training rate is obviously too high. We'll stick to a learning rate 
of 1e-3 for now.


__I finally applied augmentations to the images for speedy-sub-model12:__

```
Augments: scaled, translated, rotated, flipped, gaussian
Training images:   6208
Validation images: 1552
Model: speedy-sub-model12
[Architecture: ...]
Epochs, max: 50
Epochs, early stop: 7
Batchsize: 32
Batches: 194
Learning rate: 0.001
Dropout: 0.5
Batch normalization: True
Training time: 8478 sec = 141.3 min = 2.355 hr
```

![acc](./plots/speedy-sub-model12_acc.png)

![loss](./plots/speedy-sub-model12_loss.png)

At Epoch 8 the validation loss increased to 0.7 and early stop triggered. 
The resolution seems too low, and volatility to high, to do much anaysis here, 
but from what I can tell augmenting the images as improved the model. Only 485
images were augmented so volatility may result from this. Employing k-folding 
and some grid searching (actually, I recently read [an article](https://medium.com/rants-on-machine-learning/smarter-parameter-sweeps-or-why-grid-search-is-plain-stupid-c17d97a0e881) 
that demonstrated grid searching is often worse than random searching) should 
help me improve model parameters.


__Let's increase the learning rate back to 1e-4 for speedy-sub-model13 and see if that helps:__

```
Augments: scaled, translated, rotated, flipped, gaussian
Training images:   6208
Validation images: 1552
Model: speedy-sub-model13
[Architecture: ...]
Epochs, max: 50
Epochs, early stop: 10
Batchsize: 32
Batches: 194
Learning rate: 0.0001
Dropout: 0.5
Batch normalization: True
Training time: 10792 sec = 179.87 min = 2.998 hr
```

![acc](./plots/speedy-sub-model13_acc.png)

![loss](./plots/speedy-sub-model13_loss.png)

Again, the resolution is too low for me to even bother fitting, etc. Thus it's
only qualitatively similar to before. I should save accuracy and loss more often, 
though calling the cost function more often makes the runtime longer.


__And indeed, logging stats every batch makes runtime very slow, as seen here for speedy-sub-model14:__

```
Augments:
Training images:   388
Validation images: 97
Model: speedy-sub-model14
[Architecture: ...]
Epochs, max: 50
[Batches], early stop: 407
Batchsize: 32
Batches: 13
Learning rate: 0.0001
Dropout: 0.5
Batch normalization: True
Training time: 5424 sec = 90.4 min = 1.507 hr
```

![acc](./plots/speedy-sub-model14_acc.png)

![loss](./plots/speedy-sub-model14_loss.png)

This model was not training with augmentation enabled -- runtime would have been _even longer_.
But interestingly, when I logged model performance every batch, the model did worse. 
Or more likely, I am doing something wrong, eithere here or prior... 
From this, I should probably refactor my code to something like: 

```
for i in iterations:
  train batch
  if i % something: validate
```

Compared to, currently:
```
for e in epochs:
  for batch in batches:
    train
  validate
```

I think I understand that these implementations of TensorFlow may be outdated.
TensorFlow now (or always has?) has higher level APIs. TensorFlow seems to 
always be in flux regarding what is still kosher (not deprecated contrib) 
and what is still convention (DataSets, Estimators).
As of April 12: In any case, I need to defrag my current cnn.py.


## [Predicting](https://gitlab.com/Bonatt/WristWatchIdentification/blob/master/predict.py) <a name="predict"></a>

Post-training, the speedy-sub-model9 may be tested on a handful of images that I've curated specifically for this project.
I've also recently added some images that were originally trained on to increase statistics.
Data is currently in short supply therefore separate scraped images are currently not officially employed for testing.
<!--They can be seen in the ```predictimages``` folder, or here:-->

![predictimages](./predictimages/predictimagesfamily.png)
 
Below are the test results:

```
104predictimage1.jpg       (0.99628, 0.00372) --> speedy         INCORRECT: 104
datejustpredictimage1.jpg  (0.99845, 0.00155) --> speedy         INCORRECT: datejust
memepredictimage1.png      (0.99712, 0.00288) --> speedy         INCORRECT: meme
pelagospredictimage1.jpg   (0.91257, 0.08743) --> speedy         INCORRECT: pelagos
plutopredictimage1.jpg     (0.99826, 0.00174) --> speedy         INCORRECT: pluto
predictimagesfamily.png    (0.95945, 0.04055) --> speedy         INCORRECT:
speedy20790974.jpg         (0.98784, 0.01216) --> speedy         CORRECT
speedy20791066.jpg         (0.99173, 0.00827) --> speedy         CORRECT
speedy20791090.jpg         (0.99365, 0.00635) --> speedy         CORRECT
speedy20791440.jpg         (0.99643, 0.00357) --> speedy         CORRECT
speedy20792197.jpg         (0.98747, 0.01253) --> speedy         CORRECT
speedy20792299.jpg         (0.99545, 0.00455) --> speedy         CORRECT
speedy20792465.jpg         (0.93786, 0.06214) --> speedy         CORRECT
speedy20792654.jpg         (0.82141, 0.17859) --> speedy         CORRECT
speedy20793630.jpg         (0.93088, 0.06912) --> speedy         CORRECT
speedy20793655.jpg           (0.9685, 0.0315) --> speedy         CORRECT
speedy20793901.jpg         (0.98011, 0.01989) --> speedy         CORRECT
speedy20794025.jpg         (0.99341, 0.00659) --> speedy         CORRECT
speedy20794439.jpg         (0.99829, 0.00171) --> speedy         CORRECT
speedy20795613.jpg         (0.99774, 0.00226) --> speedy         CORRECT
speedypredictimage1.jpg      (0.99996, 4e-05) --> speedy         CORRECT
speedypredictimage2.jpg    (0.97835, 0.02165) --> speedy         CORRECT
speedypredictimage3.jpg    (0.98223, 0.01777) --> speedy         CORRECT
srppredictimage1.jpg       (0.87231, 0.12769) --> speedy         INCORRECT: srp
sub20790024.jpg            (0.03903, 0.96096) --> sub            CORRECT
sub20791031.jpg            (0.04479, 0.95521) --> sub            CORRECT
sub20791057.jpg            (0.04253, 0.95747) --> sub            CORRECT
sub20791499.jpg            (0.04684, 0.95316) --> sub            CORRECT
sub20791854.jpg            (0.04143, 0.95857) --> sub            CORRECT
sub20791894.jpg            (0.02155, 0.97845) --> sub            CORRECT
sub20792318.jpg            (0.02441, 0.97559) --> sub            CORRECT
sub20792841.jpg            (0.12704, 0.87296) --> sub            CORRECT
sub20793209.jpg            (0.05072, 0.94928) --> sub            CORRECT
sub20793291.jpg            (0.03257, 0.96743) --> sub            CORRECT
sub20793489.jpg            (0.04704, 0.95296) --> sub            CORRECT
sub20793560.jpg            (0.05791, 0.94209) --> sub            CORRECT
sub20793567.jpg            (0.03506, 0.96494) --> sub            CORRECT
sub20794058.jpg            (0.03698, 0.96302) --> sub            CORRECT
sub20795276.jpg            (0.04572, 0.95428) --> sub            CORRECT
sub20795373.jpg            (0.02955, 0.97045) --> sub            CORRECT
sub20795738.jpg            (0.04839, 0.95161) --> sub            CORRECT
sub20795958.jpg            (0.03466, 0.96534) --> sub            CORRECT
sub20796325.jpg            (0.04593, 0.95407) --> sub            CORRECT
sub20796739.jpg                (0.061, 0.939) --> sub            CORRECT
sub20797109.jpg            (0.02878, 0.97122) --> sub            CORRECT
subpredictimage1.jpg       (0.99158, 0.00842) --> speedy         INCORRECT: sub
subpredictimage2.jpg       (0.00655, 0.99345) --> sub            CORRECT
subpredictimage3.jpg       (0.99892, 0.00108) --> speedy         INCORRECT: sub
```

Images named "*207...jpg" are sample training images. All of these were correctly classified as either an image
of a Omega Speedmaster (speedy) or Rolex Submariner (sub). Whether this is overfitting or not, I am currently unsure.
Images of other watches, such as the Seiko SRP, Sinn 104, Tudor Pelados, and Rolex Datejust 
(and Pluto and a text meme), which I've included to help diagnose the model, 
were incorrectly classified as speedy only, interestingly. 
I am also currently unsure why.
The model also incorrectly identifies two of the three subpredictimages, though one of them 
is an image of _two_ GMTs (close enough to sub that the model should think they're subs?). 
This is where localization would help.


__Below are some relatively new analyses from speedy-sub-model13:__

![probhis](./predictimages/speedy-sub-model13_probhist.png)

![cm](./predictimages/speedy-sub-model13_cm.png)

Above are probability distributions for 'speedy*' and 'sub*' images, 
and a correlation/confusion matrix. Below is the classification report:

```
             precision    recall  f1-score   support

     speedy       0.94      0.84      0.89        19
        sub       0.88      0.96      0.92        24

avg / total       0.91      0.91      0.91        43
```

As of April 16 (output above April 15), no comment on report.

But in other news: look at all this data I don't have.



## Other Datasets <a name="other"></a>
I can also turn my code towards classifying cats and dogs, and even MNIST, for benchmarking.
Currently these datasets contain many more images than my watches dataset.
I have good MNIST data on my machine, but formatting it so this CNN can accept it is more work than
formatting existing cat and dog data, which I've SVN'd from [here](https://github.com/sankit1/cv-tricks.com/tree/master/Tensorflow-tutorials/tutorial-2-image-classifier/training_data). So cats and dogs it is! 
This is also available via [Kaggle](https://www.kaggle.com/c/dogs-vs-cats).

All relevant hyperdata is identical to that of speedy-sub-model9:

```
Augments:
Training images:   800
Validation images: 200
Model: cat-dog-model8
Architecture:
 input shape: (?, 128, 128, 3)
  conv layer shape: (?, 128, 128, 32)
  output max pooling layer shape: (?, 64, 64, 32)
 input shape: (?, 64, 64, 32)
  conv layer shape: (?, 64, 64, 32)
  output max pooling layer shape: (?, 32, 32, 32)
 input shape: (?, 32, 32, 32)
  conv layer shape: (?, 32, 32, 64)
  output max pooling layer shape: (?, 16, 16, 64)
 input shape: (?, 16, 16, 64)
  conv layer shape: (?, 16, 16, 64)
  output max pooling layer shape: (?, 8, 8, 64)
 input shape: (?, 8, 8, 64)
  conv layer shape: (?, 8, 8, 128)
  output max pooling layer shape: (?, 4, 4, 128)
 input shape: (?, 4, 4, 128)
  output flat layer shape: (?, 2048)
 input shape: (?, 2048)
  output fc layer shape: (?, 128)
  dropout: 0.5
 input shape: (?, 128)
  output fc layer shape: (?, 2)
Epochs, max: 50
Epochs, early stop: 28
Batchsize: 32
Batches: 25
Learning rate: 0.0001
Dropout: 0.5
Batch normalization: True
Training time: 3463 sec = 57.72 min = 0.962 hr
```

![acc](./plots/cat-dog-model8_acc.png)

![loss](./plots/cat-dog-model8_loss.png)

With much more statistics, I am surprised that validation accuracy and loss 
seem to be poor. An increased learning rate may provide better optimization.
Or has the model overfit due to too-specific of an architecture? 

Removing the final two convolutional layers produces the below results:

```
Augments:
Training images:   800
Validation images: 200
Model: cat-dog-model9
Architecture:
input shape: (?, 128, 128, 3)
 conv layer shape: (?, 128, 128, 32)
 output max pooling layer shape: (?, 64, 64, 32)
input shape: (?, 64, 64, 32)
 conv layer shape: (?, 64, 64, 32)
 output max pooling layer shape: (?, 32, 32, 32)
input shape: (?, 32, 32, 32)
 conv layer shape: (?, 32, 32, 64)
 output max pooling layer shape: (?, 16, 16, 64)
input shape: (?, 16, 16, 64)
 output flat layer shape: (?, 16384)
input shape: (?, 16384)
 output fc layer shape: (?, 128)
 dropout: 0.5
input shape: (?, 128)
 output fc layer shape: (?, 2)
Epochs, max: 50
Epochs, early stop: 23
Batchsize: 32
Batches: 25
Learning rate: 0.0001
Dropout: 0.5
Batch normalization: True
Training time: 2594 sec = 43.24 min = 0.721 hr
```

![acc](./plots/cat-dog-model9_acc.png)

![loss](./plots/cat-dog-model9_loss.png)

There may exist some improvement, or maybe it's worse. I haven't given it much
thought; I was only curious to see how my scripts ran on different data 
(i.e. how automated and independent are they?).
In any case, the model is certainly not optimized. I wonder what's up, and how I can fix it.

![loss vs epoch](https://cdn-images-1.medium.com/max/1600/0*uIa_Dz3czXO5iWyI.)

From the [above image](http://cs231n.github.io/neural-networks-3/#loss),
I'd say that the training set is learning too slow and the validation set is learning too fast. 
I don't think that makes sense, though. Is classifying cats and dogs generally harder than classifying
divers and chronographs? Maybe.


## Future Expansions <a name="future"></a>

Thoughts on future expansions... To be continued.
