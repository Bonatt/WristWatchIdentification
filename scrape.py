### To scrape images of watches from Google Images
# Haha, see this for why scraping Google is the wrong approach:
# https://stackoverflow.com/questions/36438261/extracting-images-from-google-images-using-src-and-beautifulsoup
# I will scrape from WatchRecon instead.
# The are 186x140 so I don't have to resize/reshape, and about 30-40 per every 
# 48 are useable. I manually go through them and trash the bad ones. Here, bad
# ones are images that I or a beginner cannot easily identify based on the 
# watch alone (no box, papers. bracelet, bezel insert, non-standard 
# version/colors, etc.) I've kept blue subs, but trashed most white speedies. 
# I could have queried a more specific reference but whatever. 
# Note: Rolex sellers like to show a lot of full set pictures.


from urllib.request import urlopen, urlretrieve
from bs4 import BeautifulSoup
import re
import os.path
import time



# Specify the url architecture
# e.g., https://www.watchrecon.com/?query=omega+speedmaster&current_page=1
watchrecon = 'https://www.watchrecon.com/'
query = '?query='
currentpage = '&current_page='

# Watches to scrape
# [watch/directory, query string]
data = './data/'
watches = [ ['speedy', 'omega+speedmaster'],
            ['sub', 'rolex+submariner'],
            #['skx', 'skx' ]
          ]

# Number of current_pages to scrape. There are 48 images per page.
# WatchRecon pages are time-dependen -- newer first, older last.
firstpage = 1
maxpage = 15
pages = range(firstpage, maxpage+1)




### # Get associated htlm data as soup
# From https://medium.freecodecamp.org/how-to-scrape-websites-with-python-and-beautifulsoup-5946935d93fe
# And py3 corrections https://stackoverflow.com/questions/2792650/import-error-no-module-name-urllib2
def GetSoup(site, search, p, throttle=False):
  # Specify the url
  url = site + query + search + currentpage + str(p)
  # Query the website and return the htmls
  html = urlopen(url)
  # Parse the html using beautiful soup
  soup = BeautifulSoup(html, 'html.parser')
  # Pause after url fetch. Not really necessary, I think.
  if throttle:
    time.sleep(3) # seconds
  return soup




### Get all image thumbnails urls from soup.
# From https://stackoverflow.com/questions/8289957/python-2-7-beautiful-soup-img-src-extract
# imgs_speedy[0] = 'https://www.watchrecon.com/image.php?image_id=20571574&size=med&ratio=1.33'
def GetImgs(soup):
  imgs = [watchrecon+i['src'] for i in soup.findAll('img', {'class':'thumb'})]
  return imgs




### Download images as <image_id> to assoaciated directory. Distinct loops per
# watch because what if different number of images exist? I've seen a few 
# non-existants, I think. May cause trouble.
# From https://stackoverflow.com/questions/18497840/beautifulsoup-how-to-open-images-and-download-them
# and https://stackoverflow.com/questions/32680030/match-text-between-two-strings-with-regular-expression
# After correspondance with WatchRecon support, my scraping demands
# (~1000 images required) were allowed with stipulation that I throttle my 
# retrieves "to at most 1 fetch per second".
def DownloadImgs(imgs, path, throttle=True):
  for img in imgs:
    id = re.search(r'=(.*?)&', img).group(1)
    name = path.split('/')[-2]
    # Check if file exists in data or Trash. Don't download if so.
    file_good = path+name+id+'.jpg'
    file_trash = '/home/joshua/.local/share/Trash/files/'+name+id+'.jpg'
    if os.path.exists(file_good) or os.path.exists(file_trash):
      continue
    else: 
      urlretrieve(img, file_good)
      print('   ', file_good)
    # Pause after each image download
    if throttle:
      time.sleep(3) # seconds




### Main, of course.
def main():
  for watch in watches:
    print('', watch[0])
    for page in pages:
      print('  page', page)
      Soup = GetSoup(watchrecon, watch[1], page)
      Imgs = GetImgs(Soup)
      DownloadImgs(Imgs, data+watch[0]+'/')

# Timing code for interest
t0 = time.time()
main()
t1 = time.time()
print('\nScraping time:', int(t1-t0), 'sec =', round((t1-t0)/60,2), 'min =', round((t1-t0)/3600,3), 'hr')
