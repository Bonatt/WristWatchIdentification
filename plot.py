import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from load import *

datadir = './data/'
modelname = 'speedy-sub-model15'
#datadir = './dataCATDOG/'
#modelname = 'cat-dog-model9'
modeldir = './models/'+modelname+'/'
plotdir = './plots/'

def PlotResults(log=modeldir+modelname+'.log', save=True):
  global stats
  stats = pd.read_csv(log)
  tacc, vacc, tloss, vloss = stats.values.T
  epoch = range(1,len(tacc)+1)

  plt.figure()
  plt.plot(epoch, tacc, label='Training')
  plt.plot(epoch, vacc, label='Validation')
  plt.title(modelname)
  #plt.xlabel('Epoch')
  plt.xlabel('Batch')
  plt.ylabel('Accuracy')
  plt.legend()
  if save:
    plt.savefig(plotdir+modelname+'_acc.png')
  plt.show(block=False)

  plt.figure()
  plt.plot(epoch, tloss, label='Training')
  plt.plot(epoch, vloss, label='Validation')
  plt.title(modelname)
  #plt.xlabel('Epoch')
  plt.xlabel('Batch')
  plt.ylabel('Loss')
  plt.legend()
  if save:
    plt.savefig(plotdir+modelname+'_loss.png')
  plt.show(block=False)

PlotResults()




### Display dim = n*n images of one or all classes
# Only plots training images
def ImageGrid(datadir=datadir, ID='', dim=-1, rand=True, save=True):
  # Load all, shuffle, then pick dim*dim images
  if rand:
    data = LoadImages(path=datadir+ID)
    #np.random.seed(0)
    np.random.shuffle(data)
    data = data[:dim*abs(dim)]
  # Just load dim*dim images
  else:
    data = LoadImages(path=datadir+ID, nimages=dim*abs(dim))
  imgs, labels, names, classes = data.T

  # Image dimensions (assumed square image)
  size = imgs[0].shape[0]

  # Restransform dim from potentially -1 to actual value for image grid
  dim = int(np.sqrt(imgs.shape[0]))
  
  # "Plot" images in grid, shifting where to "plot" for each image
  display = np.zeros([dim*size, dim*size, imgs[0].shape[-1]], dtype = np.float32)
  for i in range(dim):
    for j in range(dim):
      img = imgs[i*dim + j]
      disp_padded = np.pad(img, ((i*size, (dim-1-i)*size),
                                 (j*size, (dim-1-j)*size), (0,0)), 
                                 'constant')
      display = np.add(display, disp_padded)

  plt.figure()#figsize = (dim * 3, dim * 3))
  plt.imshow(display.astype(np.uint8))
  #plt.title(ID)
  plt.axis('off')
  if save:
    plt.savefig(plotdir+ID+'family_n='+str(int(dim*dim))+'.png', bbox_inches='tight')
  plt.show(block=False)

# Plot all watches, all images, shuffled
#ImageGrid()

# Plot only speedy, all images, not shuffled
#ImageGrid(ID='speedy', rand=False)

# Plot only sub, all images, not shuffled
#ImageGrid(ID='sub', rand=False)

# Plot only sub, one image, shuffled
#ImageGrid(ID='sub', dim=1, rand=True)
