### CNN for Wrist Watch Identification
# Currently only two references to train: Omega Speedmaster and Rolex Submariner
# Followed http://cv-tricks.com/tensorflow-tutorial/training-convolutional-neural-network-for-image-classification/

from load import *
import tensorflow as tf
import numpy as np
import time
import matplotlib.pyplot as plt

# Setting random seed for reproducibility
np.random.seed(0)
tf.set_random_seed(0)


###############################################################################
# CODE I A MESS DUE TO DEBUGGING, UPGRADING AND OPTIMIZATION CAMPAIGNS (LEARNING)
# TODO: refactor code and include TensorBoard.
###############################################################################


# Loaded in main, at bottom. Just kidding -- need it for some variables sooner
# I could create more fuctions/classes to fix this, but not right now.
# Training with augmented images (~200 vs ~4000) takes much, much longer.
def GetDatasets(path, size, \
                augment=False, 
                augments=['scaled', 'translated', 'rotated', 'flipped', 'gaussian']):
  if augment:
    # https://stackoverflow.com/questions/5598181/python-multiple-prints-on-the-same-line
    print('\nLoading images and creating augmented images... ', end='', flush=True)
  else:
    print('\nLoading images... ', end='', flush=True)
  train, valid = Datasets(path, size, augment=augment, augments=augments)
  print('Done.')
  if augment:
    print('Augments:', ', '.join(augments))

  # data[0] = [img0, label0, name0, class0], etc.
  #  --> imgs, labels, names, classes = data
  train = train.T
  valid = valid.T
  
  print('\nTraining images:  ', train.shape[1])
  print(  'Validation images:', valid.shape[1])
 
  return train, valid

### Speedy, sub data...
#'''
PATH = './data/'
SIZE = 128 #140
classes, nclasses = GetClasses(PATH)
nchannels = len('RGB')
modelname = 'speedy-sub-model15'
modeldir = './models/'+modelname+'/'
# Available augments: ['scaled', 'translated', 'rotated', 'flipped', 'gaussian']
A = False
if A:
  augments = ['scaled', 'translated', 'rotated', 'flipped', 'gaussian']
  train, valid = GetDatasets(PATH, SIZE, augment=True, augments=augments)
else:
  augments = ['']
  train, valid = GetDatasets(PATH, SIZE)
#'''
### Try loading csv MNIST data for diagnostics...
# Nevermind (for now?). Formatting everything is too much work right now.
'''
PATH = './dataMNISTCSV/'
SIZE = 128
nclasses = 10
nchannels = 3 #1
modelname = 'mnist-model1'
modeldir = './models/'+modelname+'/'
augments = ['']
train, valid = GetDatasets(PATH, SIZE)
'''
### Use cv-tricks' cat and dog images instead...
'''
PATH = './dataCATDOG/'
SIZE = 128
classes, nclasses = GetClasses(PATH)
nchannels = len('RGB')
modelname = 'cat-dog-model9'
modeldir = './models/'+modelname+'/'
augments = ['']
train, valid = GetDatasets(PATH, SIZE)
'''



### To create weights, baises
def SetWeights(shape):
  return tf.Variable(tf.truncated_normal(shape, stddev=0.05))
def SetBiases(size):
  return tf.Variable(tf.constant(0.05, shape=[size]))




### To create layers of network
# Conv layer followed by pooling, rolled into one "layer"
def SetConvLayer(input, nchannels_input, filtersize_conv, nfilters, bn, training):
  print('input shape:', input.shape)
  LogArch('input shape: '+str(input.shape))
  weights = SetWeights(shape=[filtersize_conv, filtersize_conv, nchannels_input, nfilters])
  biases = SetBiases(nfilters)
  # Creating the convolutional layer: w*x + b
  # [batch_stride, x_stride, y_stride, depth_stride]
  layer = tf.nn.conv2d(input=input, filter=weights, strides=[1, 1, 1, 1], padding='SAME')
  layer += biases
  print(' conv layer shape:', layer.shape)
  LogArch(' conv layer shape: '+str(layer.shape))
  # Batch normalization
  # https://www.tensorflow.org/api_docs/python/tf/layers/batch_normalization
  # https://stackoverflow.com/questions/43234667/tf-layers-batch-normalization-large-test-error
  if bn:
    layer = tf.layers.batch_normalization(layer, training=training)
  # Max-pooling
  layer = tf.nn.max_pool(value=layer, ksize=[1, 2, 2, 1], strides=[1, 2, 2, 1], padding='SAME')
  # Output of pooling fed to ReLU activation function
  layer = tf.nn.relu(layer)
  print(' output max pooling layer shape:', layer.shape)
  LogArch(' output max pooling layer shape: '+str(layer.shape))
  return layer

def SetFlatLayer(layer):
  print('input shape:', layer.shape)
  LogArch('input shape: '+str(layer.shape))
  # Shape of the layer will be [batchsize, SIZE SIZE nchannels]
  # But let's get it from the previous layer
  layer_shape = layer.get_shape()
  # Number of features is SIZE*SIZE*nchannels
  # (?, 8, 8, 64)[1:] = (8, 8, 64)
  nfeatures = layer_shape[1:].num_elements()
  # Flatten the layer
  layer = tf.reshape(layer, [-1, nfeatures])
  print(' output flat layer shape:', layer.shape)
  LogArch(' output flat layer shape: '+str(layer.shape))
  return layer

def SetFCLayer(input, ninputs, noutputs, relu, bn, training):
  print('input shape:', input.shape)
  LogArch('input shape: '+str(input.shape))
  # Define trainable weights, biases
  weights = SetWeights(shape=[ninputs, noutputs])
  biases = SetBiases(noutputs)
  # Fully connected layer takes input x and produces w*x + b. 
  # Since these are matrices we use matmul (matrix multiply-ication)
  # TODO: Put dropout here instead of after in layer set?
  # See https://www.youtube.com/watch?v=3bownM3L5zM ~1:20
  # layer = Dropout(layer)...
  layer = tf.matmul(input, weights) + biases
  if bn:
    layer = tf.layers.batch_normalization(layer, training=training)
  if relu:
    layer = tf.nn.relu(layer)
  print(' output fc layer shape:', layer.shape)
  LogArch(' output fc layer shape: '+str(layer.shape))
  return layer

def SetDropout(layer, keep_prob):
  dropout = tf.nn.dropout(layer, keep_prob)
  print(' dropout:', keep_prob_t)
  LogArch(' dropout: '+str(keep_prob_t))
  return dropout


### Log architecture
arch = []
def LogArch(string):
  arch.append(string)




### Network will have three convolution layers (followed by flat pooling layer), 
# and two fully connected layers
# I pushed all these variables into a function and set variables to global...
def SetCNN():
  # Initial tf graph placeholders for inputs
  global x
  x = tf.placeholder(tf.float32, shape=[None, SIZE, SIZE, nchannels], name = 'x')
  print('\nArchitecture:')
  #print('x shape:', x.shape, '\n')
  global y_true
  y_true = tf.placeholder(tf.float32, shape=[None, nclasses], name = 'y_true')
  global y_true_class
  y_true_class = tf.argmax(y_true, axis=1)

  # Network graph parameters and graph actual
  # batch normalization training bool
  global bn
  bn = True
  global training
  training = tf.placeholder(tf.bool, name='training')

  # filtersize = n*n
  filtersize_conv1 = nchannels # conv = convolution
  nfilters_conv1 = SIZE//4
  layer_conv1 = SetConvLayer(input = x,
                             nchannels_input=nchannels,
                             filtersize_conv = filtersize_conv1,
                             nfilters = nfilters_conv1,
                             bn = bn,
                             training = training)

  filtersize_conv2 = nchannels
  nfilters_conv2 = SIZE//4
  layer_conv2 = SetConvLayer(input = layer_conv1,
                             nchannels_input = nfilters_conv1,
                             filtersize_conv = filtersize_conv2,
                             nfilters = nfilters_conv2,
                             bn = bn,
                             training = training)

  filtersize_conv3 = nchannels
  nfilters_conv3 = SIZE//2
  layer_conv3 = SetConvLayer(input = layer_conv2,
                             nchannels_input = nfilters_conv2,
                             filtersize_conv = filtersize_conv3,
                             nfilters = nfilters_conv3,
                             bn = bn,
                             training = training)
  #'''
  filtersize_conv4 = nchannels
  nfilters_conv4 = SIZE//2
  layer_conv4 = SetConvLayer(input = layer_conv3,
                             nchannels_input = nfilters_conv3,
                             filtersize_conv = filtersize_conv4,
                             nfilters = nfilters_conv4,
                             bn = bn,
                             training = training)

  filtersize_conv5 = nchannels
  nfilters_conv5 = SIZE#//1 # Can I do this??
  layer_conv5 = SetConvLayer(input = layer_conv4,
                             nchannels_input = nfilters_conv4,
                             filtersize_conv = filtersize_conv5,
                             nfilters = nfilters_conv5,
                             bn = bn,
                             training = training)
  #'''
  layer_flat = SetFlatLayer(layer_conv5)

  fc_layer_size = SIZE # fc = fully-connected
  layer_fc1 = SetFCLayer(input = layer_flat,
                         ninputs = layer_flat.get_shape()[1:].num_elements(),
                         noutputs = fc_layer_size,
                         relu = True,
                         bn = bn,
                         training = training)
  # Dropout
  # https://stackoverflow.com/questions/40879504/how-to-apply-drop-out-in-tensorflow-to-improve-the-accuracy-of-neural-network
  global keep_prob
  keep_prob = tf.placeholder(tf.float32, name='keep_prob')
  #dropout = tf.nn.dropout(layer_fc1, keep_prob, name='dropout')
  layer_fc1_dropout = SetDropout(layer_fc1, keep_prob)

  # Final layer
  global layer_fc2
  layer_fc2 = SetFCLayer(input = layer_fc1_dropout, #dropout, #layer_fc1,
                         ninputs = fc_layer_size,
                         noutputs = nclasses,
                         relu = False,
                         bn = False,
                         training = training)

  # Final tf graph placeholders for output layer
  #global y_pred
  y_pred = tf.nn.softmax(layer_fc2, name = 'y_pred')
  global y_pred_class
  y_pred_class = tf.argmax(y_pred, axis=1)




### Progress output and append to list
### TODO: include precion, recall, f1 score
# Instead of logging stats every epoch I COULD log every iteration... below.
def Progress(sess, feed_dict_t, feed_dict_v, accuracy, train_loss, valid_loss):
  train_acc = sess.run(accuracy, feed_dict = feed_dict_t)
  valid_acc = sess.run(accuracy, feed_dict = feed_dict_v)
  t = str(round((time.time()-t0)/60,2))+' min'
  s = '\tTraining Acc.: {:>7.2%} | Val. Acc.: {:>7.2%} | Training Loss: {:.4f} | Val. Loss: {:.4f} | '+t
  print(s.format(train_acc, valid_acc, train_loss, valid_loss))
  # Tip: acc%(1/setsize) = 0 if acc is quanta of setsize, e.g. 56.25%(1/32) = 0




### Log stats to list
# This could be put into Progress
stats = []
def LogStats(sess, feed_dict_t, feed_dict_v, accuracy, train_loss, valid_loss):
  train_acc = sess.run(accuracy, feed_dict = feed_dict_t)
  valid_acc = sess.run(accuracy, feed_dict = feed_dict_v)
  stats.append([train_acc, valid_acc, train_loss, valid_loss])




### Running Average
# https://stackoverflow.com/questions/13728392/moving-average-or-running-mean
'''
def running_mean(x, N):
    cumsum = np.cumsum(np.insert(x, 0, 0)) 
    return (cumsum[N:] - cumsum[:-N]) / float(N)
'''

### Early stop check
# https://stats.stackexchange.com/questions/231061/how-to-use-early-stopping-properly-for-training-deep-neural-network
# Stop when <check> starts to increase. Should probably try to stop it when it
# stops decreasing because model does continue to fit while <check> slowly starts
# to increase...
# TODO: Add acc. not increasing (some amount) after patience epochs
'''
#stats_kernel = []
def EarlyStopCheck(train_acc, valid_acc, train_loss, valid_loss, tocheck=3, patience=5):
    # If mean of prior <patience> <tocheck> is less than current <tocheck>, break
    # stats = [tacc, vacc, tloss, vloss]
    #s = []
    check = stats[:][tocheck]
    check_kernel = vloss[-patience:]
    check_kernel_mean = np.mean(check_kernel, axis=0)
    if valid_loss > check_kernel_mean:
      return True
    else:
      return False
'''
def EarlyStopCheck(ON=True, tocheck='valid_loss', patience=10):
  if ON:
    # Moved EarlyStopCheck BEFORE stats save (to not save tripped epoch), but
    # then stats is empty on first check. Give this logic here to not check on 
    # on first iteration.
    if len(stats) == 0:
      return False
    str2index = {'train_acc':0, 'valid_acc':1, 'train_loss':2, 'valid_loss':3}
    #check = stats[:][str2index[tocheck]]
    check = np.array(stats).T[str2index[tocheck]]
    # Do not include current epoch <check> in kernel mean:
    # This goes many longer and increases code complexity here...
    #'''
    # On first epoch will throw mean of empty slice warning and show nan
    if len(check[-patience:-1]) == 0:
      check_kernel = check[-patience:]
    else:
      check_kernel = check[-patience:-1]
    #'''
    # Do include current epoch:
    #check_kernel = check[-patience:]
    check_kernel_mean = np.mean(check_kernel, axis=0)
    pad = ' '*len('Epoch 1 training...')+'\t'
    #s = pad+'{}_kernel (patience = {}), _epoch: {:.4f}, {:.4f}'
    s = pad+'{}: mean of prev. {}, curr.: {:.4f}, {:.4f}'
    #print(check_kernel_mean, check[-1])
    print(s.format(tocheck, min(patience, len(stats)-1), check_kernel_mean, check[-1]))
    if check[-1] > check_kernel_mean:
      print(pad+'Early stop!')
      # ~backtrack one epoch by popping last of stats off
      #stats.pop()
      del stats[-1]
      return True
    else:
      return False
  else:
    return False





### Train function
# TODO: cross-validation, grid search
def Train(sess, cost, optimizer, accuracy, epochs, batchsize):
  global nbatches
  nbatches = ceil(train.shape[1]/batchsize)
  print('\nModel:', modelname)
  print('Epochs:', epochs)
  print('Batchsize:', batchsize)
  print('Batches:', nbatches)
  print('Learning rate:', learningrate)
  print('Dropout:', keep_prob_t)
  print('Batch normalization:', bn, '\n')

  for epoch in range(epochs):
    print('Epoch '+str(epoch+1)+' training...', end='', flush=True)
    for batch in range(nbatches):
      # 'New' takes from train and valid, and valid runs out of instances, then throws error during cost
      # Old doesn't use all data, and overlaps evey batch. May be cause of sawtooth 
      # Fix this mess.
      # Old:
      #i1 = batch
      #i2 = batch+batchsize
      # New:
      '''
      i1 = batch*batchsize
      i2 = batch*batchsize + batchsize
      # Training batchsize to validation batchsize. This is clearly the wrong approach...
      vbatchsize = floor(batchsize*valid.shape[1]/train.shape[1])
      vi1 = batch*vbatchsize
      vi2 = batch*vbatchsize + vbatchsize
      '''
      # New2: make val batch wrap around? This works once around, then 0-32 forever.
      '''
      i1 = batch*batchsize
      i2 = batch*batchsize + batchsize
      vi1 = batch*batchsize
      vi2 = batch*batchsize + batchsize
      if i1 > valid.shape[1]:
        vi1 = 0
        vi2 = batchsize
      '''
      # New3:
      #for batch in range(nbatches): [(batch*batchsize)%valid.shape[1], (batch*batchsize)%valid.shape[1]+batchsize]
      # Will not train/validate up to batchsize-1 images because of the reset to preserve batchsize equality
      # i.e. no batch of 4 images at the end, e.g.
      # So final batch of batches will hav len32 instead of 4. So training
      # acc, loss will be dividen of 32, not 4.
      #'''
      i1 = (batch*batchsize)%train.shape[1]
      i2 = i1 + batchsize
      if i2 >= train.shape[1]:
        continue
        #i1 = 0
        #i2 = batchsize
      '''
      vi1 = i1%valid.shape[1]
      vi2 = vi1 + batchsize #i2%valid.shape[1] # i1%valid.shape[1] + batchsize
      if vi2 >= valid.shape[1]:
        vi1 = 0
        vi2 = batchsize
      '''

      # [images, labels, names, class]
      x_t, y_true_t = train[0][i1:i2], train[1][i1:i2]
      #x_v, y_true_v = valid[0][vi1:vi2], valid[1][vi1:vi2]

      # Reshape from (batchsize,) to...
      x_t = np.array([i for i in x_t])           # (batchsize, SIZE, SIZE, nchannels)
      y_true_t = np.array([i for i in y_true_t]) # (batchsize, nclasses)
      #x_v = np.array([i for i in x_v])
      #y_true_v = np.array([i for i in y_true_v])

      ### It's x_v.shape = (0,) and y_true_v.shape = (0,) that makes problems.
      # Create it some other time, ie every epoch instead of every batch?
      # Or make some variable vbatchsize to not run out of val instances
      #st = 'TRAIN: [{}:{}], x={}, y={}'
      #sv = 'VALID: [{}:{}], x={}, y={}'
      #print(st.format(i1, i2, x_t.shape, y_true_t.shape))#, ' | ', sv.format(vi1, vi2, x_v.shape, y_true_v.shape)) 
 
      # Create feeding dicts
      feed_dict_t = {x: x_t, y_true: y_true_t, keep_prob: keep_prob_t, training: True} # 0.5
      #feed_dict_v = {x: x_v, y_true: y_true_v, keep_prob: 1.0}

      # Optimize model from training data.
      # I read somewhere that feed_dict is the least efficient way to use tf.
      #sess.run(optimizer, feed_dict = feed_dict_t)
      # Updates mean, variance after batch normalization
      # https://www.tensorflow.org/api_docs/python/tf/layers/batch_normalization
      #with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
        #sess.run(optimizer, feed_dict = feed_dict_t)
      extra_update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
      sess.run([optimizer, extra_update_ops], feed_dict = feed_dict_t)

      ### Log stats every iteration/batch (as opposed to every epoch)
      #stats.append([acc,valid_acc, train_loss, valid_loss])
      # Calculate training loss from training data
      #train_loss = sess.run(cost, feed_dict = feed_dict_t)
      # Calculate validation loss from validation data
      #valid_loss = sess.run(cost, feed_dict = feed_dict_v)
      vi1 = i1%valid.shape[1]
      vi2 = vi1 + batchsize
      if vi2 >= valid.shape[1]:
        vi1 = 0
        vi2 = batchsize
      #x_v, y_true_v = valid[0][vi1:vi2], valid[1][vi1:vi2]
      x_v, y_true_v = valid[0], valid[1]
      x_v = np.array([i for i in x_v])
      y_true_v = np.array([i for i in y_true_v])
      feed_dict_v = {x: x_v, y_true: y_true_v, keep_prob: 1.0, training: False}
      valid_loss = sess.run(cost, feed_dict = feed_dict_v)
      
      train_loss = sess.run(cost, feed_dict = feed_dict_t)

      LogStats(sess, feed_dict_t, feed_dict_v, accuracy, train_loss, valid_loss)
      # Calculating the train,valid_loss every iter takes twice as long for ~300 images
   
    # Why did I have val stuff in batch look when it wasn't used? Moved to epoch.
    # Shuffle valid before putting into dict so dict can pull different images 
    # and not overfit? Is that happening?
    # Actually, it seems to make valacc stay at 50% when shuffling x_v, e.g.. Why? 
    # Because I shuffled them independently... duh.
    # Shuffling v=valid instead.. "ValueError: could not convert string to float: 'dog.93.jpg'". huh..
    #v = valid
    #np.random.shuffle(v)
    #x_v, y_true_v = v[0], v[1]
    '''
    x_v, y_true_v = valid[0], valid[1]

    x_v = np.array([i for i in x_v])
    y_true_v = np.array([i for i in y_true_v])
    #np.random.shuffle(x_v)    
    #np.random.shuffle(y_true_v)

    #sv = 'VALID: x={}, y={}'
    #print(sv.format(x_v.shape, y_true_v.shape))

    ### TODO: Despite 200 val images into feed_dict, valid_loss/acc is only 
    # calculated from 32 images.
    feed_dict_v = {x: x_v, y_true: y_true_v, keep_prob: 1.0, training: False}
    #print()
    #print(feed_dict_t.keys())
    #print(feed_dict_v.keys())
    # Calculate training loss from training data
    train_loss = sess.run(cost, feed_dict = feed_dict_t)
    # Calculate validation loss from validation data
    valid_loss = sess.run(cost, feed_dict = feed_dict_v)
    '''
    # Print progress, accuracy, etc.
    Progress(sess, feed_dict_t, feed_dict_v, accuracy, train_loss, valid_loss)

    # Log acc, loss stats for later save2file and analysis
    #LogStats(sess, feed_dict_t, feed_dict_v, accuracy, train_loss, valid_loss)    

    # Early stop check. Default: if valid_loss increases significantly
    if EarlyStopCheck(patience=10*nbatches): break

    # Save model after each epoch. It overwrites.
    tf.train.Saver().save(sess, modeldir+modelname)

# TODO: Move tf variables outside main? Rearrange everything to be better?!
### Let's do some training!
def main():
  global epochs
  epochs = 50
  ### TODO: should I determine nbatches instead to not waste clipped data?
  global batchsize
  batchsize = 32
  #global nbatches
  #nbatches = ceil(train.shape[1]/batchsize) #int(round(train.shape[1]/batchsize))
 
  #print('Model:', modelname) 
  #print('Epochs:', epochs)
  #print('Batchsize:', batchsize)
  #print('Batches:', nbatches, '\n')

  global learningrate
  learningrate = 1e-3 #2=too large, #3= good?, #4 = too slow?

  global keep_prob_t
  keep_prob_t = 0.5

  #global bn
  #bn = True

  with tf.Session() as sess:
    # Build CNN
    SetCNN()
    # To optimize
    cross_entropy = tf.nn.softmax_cross_entropy_with_logits_v2(logits = layer_fc2, labels = y_true)
    cost = tf.reduce_mean(cross_entropy)
    optimizer = tf.train.AdamOptimizer(learning_rate = learningrate).minimize(cost)
    # To evaluate
    correct_pred = tf.equal(y_pred_class, y_true_class)
    accuracy = tf.reduce_mean(tf.cast(correct_pred, tf.float32))
    # Initialize tf variables above
    sess.run(tf.global_variables_initializer())
    # Go
    Train(sess, cost, optimizer, accuracy, epochs, batchsize)#, nbatches)

# Timing code for interest
t0 = time.time()
main()
t1 = time.time()
tt = 'Training time: '+str(int(t1-t0))+' sec = '+str(round((t1-t0)/60,2))+' min = '+str(round((t1-t0)/3600,3))+' hr'
print('\n'+tt)




### Training, valdation accuracy, loss saved to new file
with open(modeldir+modelname+'.log', 'w') as f:
  None
with open(modeldir+modelname+'.log', 'a') as f:
  head = 'Training Accuracy, Validation Accuracy, Training Loss, Validation Loss'
  print(head, file=f)
  for line in stats:
    print('{}, {}, {}, {}'.format(*line), file=f)

### Hyperdata to new file, too
with open(modeldir+modelname+'_hyperdata.txt', 'w') as f:
  None
with open(modeldir+modelname+'_hyperdata.txt', 'a') as f:
  print('Augments: '+str(', '.join(augments)), file=f)
  print('Training images:   '+str(train.shape[1]), file=f)
  print('Validation images: '+str(valid.shape[1]), file=f)
  print('Model: '+str(modelname), file=f)
  print('Architecture:', file=f)
  for line in arch:
    print(' '+line, file=f)
  print('Epochs, max: '+str(epochs), file=f)
  print('Epochs, early stop: '+str(len(stats)), file=f)
  print('Batchsize: '+str(batchsize), file=f)
  print('Batches: '+str(nbatches), file=f)
  print('Learning rate: '+str(learningrate), file=f)
  print('Dropout: '+str(keep_prob_t), file=f)
  print('Batch normalization: '+str(bn), file=f)
  print(tt, file=f)
