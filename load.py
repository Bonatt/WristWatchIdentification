import os
import cv2
import numpy as np
from augment import *


# Top directory for scraped images
#PATH = './data/'
#PATH = './dataCATDOG/'

# Square image w,h in pixels. Smallest side of scraped images: 186x140
#SIZE = 128 #140




### Collect all file paths in and below parent path
def GetAllFiles(path): 
  #path = './data/'  == './data' ~= 'data/' == 'data'
  # Get filenames and prepend directory paths
  files = [[dirs+'/'+file for file in files] for \
            dirs, subdirs, files in os.walk(path)]
  # Flatten paths
  files = [item for sublist in files for item in sublist]
  # Only get image files
  # https://stackoverflow.com/questions/4843158/check-if-a-python-list-item-contains-a-string-inside-another-string
  matchers = ['jpg', 'png']
  files = [file for file in files for ext in matchers if ext in file]
  return files




### Get class names and number of classes
def GetClasses(path):
  classes = os.listdir(path)
  nclasses = len(classes)
  return classes, nclasses




### Transform 'speedy' to np.array([1.,0.]), e.g. Return dict to use.
def Classes2Labels(path):#, file):
  classes, nclasses = GetClasses(path)
  # Keys are Sentiment scores
  keys = classes
  # Values are np.zeros with 1 at appropriate index
  values = []
  for i in range(nclasses):    
    #print(i+1,'/',len(printable))    
    value = np.zeros(nclasses)
    value[i] = 1    
    values.append(value)#np.array(value).astype(int))
  dict = {}
  for i in range(len(keys)):    
    dict[keys[i]] = values[i]      
  return dict

# plot.ImageGrid() doesn't work unless this is uncommented,
# and below LoadImages line is uncommented to use it.    
#CLASS2LABEL = Classes2Labels(PATH)




### Load and resize to size*size image
def LoadImages(path, size, predict=False, nimages=int(1e6)):
  # data = [image, label, name, class]
  # image is array of numbers, label is array representing class name
  # name is filename, and class is speedy, eg.
  data = []

  CLASS2LABEL = Classes2Labels(path)

  # [:nimages] drops last element... Instead of messing with splicing,
  # just make nimages=-1 --> nimages=<somelargenumber>=int(1e6)?
  for file in GetAllFiles(path)[:nimages]:
    image = cv2.imread(file)
    #print(file, np.shape(image))
    # BGR --> RGB
    # https://stackoverflow.com/questions/39316447/opencv-giving-wrong-color-to-colored-images-on-loading
    image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
    image = cv2.resize(image, (size, size)).astype(np.float32)

    if predict:
      name = os.path.basename(file)
      data.append([image,name])
    else:
      classs, name = file.split('/')[-2:]
      label = CLASS2LABEL[classs]#.astype(np.float32)
      #label = Classes2Labels(path)[classs]
      data.append([image,label,name,classs])

  # Return transpose of data so data[0] = images, data[1] = labels, etc.
  # instead of data[0] = [image0, label0, etc.]
  return np.array(data)#.T




### Some kind of wrapper around augment functions to keep associated metadata
# because each function handles the image data differently.
# Assume only single *args is ever given because scale only takes first/one.
def Augment(data, func, size, *args):

  imgs, labels, names, classes = data.T
  
  if func == 'central_scale_images':
    scales = args[0]
    nscales = len(args[0])
    # Scale image data len(args) ways to return form, e.g.,
    # [img0_sc0, img0_sc1, ..., img1_sc0, img1_sc1, ...]
    imgs_aug = central_scale_images(imgs, scales, size)
    # extend metadata in same manner as image data
    labels_aug = np.repeat(labels, nscales)
    names_aug = np.repeat(names, nscales)
    classes_aug = np.repeat(classes, nscales)
    # Add scaled factor to image name  
    for i,n in enumerate(names_aug):
      #names_aug[i] = n.replace('.','_sc'+str(scales[i%nscales])+'.')
      names_aug[i] = n.replace('.','_sc'+str(i%nscales)+'.')
    # Append all items together so data[0] = [imgs0, names0, names0, classes0]
    data_aug = np.array(list(zip(imgs_aug, labels_aug, names_aug, classes_aug)))
  
  elif func == 'translate_images':
    # Translate image data in 4 directions to return form, e.g.,
    # [img0_left, img1_left, ..., img0_right, img1_right, ...]. I think.
    # reshape from (N,) to (N, 140, 140, 3)
    imgs = np.array([i for i in imgs])
    imgs_aug = translate_images(imgs, size)
    # extend metadata in the same as image data
    ntranslations = 4
    labels_aug = np.tile(labels, ntranslations)
    names_aug = np.tile(names, ntranslations)
    classes_aug = np.tile(classes, ntranslations)
    # Add translated i to image name
    for i,n in enumerate(names_aug):
      names_aug[i] = n.replace('.','_tr'+str(i%ntranslations)+'.')
    # Append all items together so data[0] = [imgs0, names0, names0, classes0]
    data_aug = np.array(list(zip(imgs_aug, labels_aug, names_aug, classes_aug)))

  elif func == 'rotate_images':
    # Rotate image 3 times to return form, e.g.,
    # [img0_90, img0_180, ..., img1_90, img1_180, ...]. I think.
    imgs_aug = rotate_images(imgs, size)
    # extend metadata in the same as image data
    nrotations = 3
    labels_aug = np.repeat(labels, nrotations)
    names_aug = np.repeat(names, nrotations)
    classes_aug = np.repeat(classes, nrotations)
    # Add rotated i to image name
    for i,n in enumerate(names_aug):
      names_aug[i] = n.replace('.','_ro'+str(i%nrotations)+'.')
    # Append all items together so data[0] = [imgs0, names0, names0, classes0]
    data_aug = np.array(list(zip(imgs_aug, labels_aug, names_aug, classes_aug)))
 
  elif func == 'flip_images':
    # Flip image 3 times to return form, e.g.,
    # [img0_left, img1_left, ..., img0_transpose, img1_transpose, ...]. I think.
    imgs_aug = flip_images(imgs, size)
    # extend metadata in the same as image data
    nflips = 3
    labels_aug = np.repeat(labels, nflips)
    names_aug = np.repeat(names, nflips)
    classes_aug = np.repeat(classes, nflips)
    # Add flipped i to image name
    for i,n in enumerate(names_aug):
      names_aug[i] = n.replace('.','_fl'+str(i%nflips)+'.')
    # Append all items together so data[0] = [imgs0, names0, names0, classes0]
    data_aug = np.array(list(zip(imgs_aug, labels_aug, names_aug, classes_aug)))

  elif func == 'add_gaussian_noise':
    # Simulate lighting in image to return form, e.g.,
    # [img0_g, img1_g, ...]. I think.
    imgs_aug = add_gaussian_noise(imgs)
    # extend metadata in the same as image data
    labels_aug = labels
    names_aug = names
    classes_aug = classes
    # Add ga to image name
    for i,n in enumerate(names_aug):
      names_aug[i] = n.replace('.','_ga.')
    # Append all items together so data[0] = [imgs0, names0, names0, classes0]
    data_aug = np.array(list(zip(imgs_aug, labels_aug, names_aug, classes_aug)))

  else: print('No bueno')

  return data_aug



### TODO: Normalize images?
# https://stats.stackexchange.com/questions/185853/why-do-we-need-to-normalize-the-images-before-we-put-them-into-cnn
# "The recommended preprocessing is to center the data to have mean of zero, 
# and normalize its scale to [-1, 1] along each feature"
# https://stats.stackexchange.com/questions/211436/why-do-we-normalize-images-by-subtracting-the-datasets-image-mean-and-not-the-c?rq=1
# I could scale and normalize here or do batch normalization in cnn
def Normalize(data):
  #m = 255.0
  None




### Create two datasets -- train, valid -- of raw images and augmented images
#AUGMENTS = ['scaled', 'translated', 'rotated', 'flipped', 'gaussian']
def Datasets(path, size, validation_percent=20, \
             augment=False, 
             augments=['scaled', 'translated', 'rotated', 'flipped', 'gaussian'],
             nimages=int(1e6)):
  data = LoadImages(path, size, nimages=nimages)

  # Augment raw images to artificially increase statistics.
  # If N = instances in raw data, and M = instances in augmented data
  # M = N * (1 + nscalings + ntranslations + nrotations + nflips + ngaussians)
  # E.g. N = 271 --> M = 4336. That's a lot of (not necessarily good) statistics
  if augment:
    # Fill dictionary
    augdict = {'scaled': Augment(data, 'central_scale_images', size, [0.90, 0.75, 0.50, 0.25]),
               'translated': Augment(data, 'translate_images', size),
               'rotated': Augment(data, 'rotate_images', size),
               'flipped': Augment(data, 'flip_images', size),
               'gaussian': Augment(data, 'add_gaussian_noise', size)}

    # Must parse it out, as I always seem to do.
    augdata = [augdict[a] for a in augments]
    data = np.concatenate((data, [item for sublist in augdata for item in sublist]))

  # Shuffle data in place
  np.random.shuffle(data)  

  # Split data into training and validation datasets
  # validation_percent is actual percent e.g. 10 not 0.10
  validation_size = int(round(data.shape[0]*validation_percent/100.))
  train = data[validation_size:]
  valid = data[:validation_size]

  return train, valid

#data = LoadImages(PATH, SIZE)
#imgs, labels, names, classes = data.T


'''
import pandas as pd
train, valid = Datasets('./data/', 128)

vdf = pd.DataFrame(valid, columns = ['imgs', 'labels', 'names', 'classes'])
vdf.classes.value_counts()

tdf = pd.DataFrame(train, columns = ['imgs', 'labels', 'names', 'classes'])
'''




### Compare augment import (tf) vs the one here (cv) 
# cv has better resolution (subdial hands visible) but
# tf has correct color (blue vs brown strap, e.g.)
# https://stackoverflow.com/questions/39316447/opencv-giving-wrong-color-to-colored-images-on-loading
# "OpenCV uses BGR as its default colour order for images, matplotlib uses RGB.
# When you display an image loaded with OpenCv in matplotlib the channels will 
# be back to front." I guess it's fine, and visualizing is what makes it wonky.
'''
# mine
i = 0
print(names[i])
plt.imshow(imgs[i].astype(np.uint8))
plt.show(block=False)

# theirs
print(os.path.basename(X_img_paths[i]))
plt.imshow(X_imgs[i].astype(np.uint8))
plt.show(block=False)

# mine augmented
print(names[i])
func = 'flipped'
print(eval(func+'[i][2]'))
plt.imshow(eval(func+'[i][0]').astype(np.uint8))
plt.show(block=False)
'''
