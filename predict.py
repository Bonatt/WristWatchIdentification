from load import *
import matplotlib.pyplot as plt
from sklearn.metrics import classification_report
from sklearn.metrics import confusion_matrix


# Which model to use? Currently...
# 'speedy-sub-model': SIZE=140 and CNN=[35,35,70,140,2]
# 'speedy-sub-model2': SIZE=128 and CNN=[32,32,64,128,2] and AUGMENTS=[:]
# 'speedy-sub-model3': SIZE=128 and CNN=[32,32,64,128,2] and AUGMENTS=[:] and dropout=0.5
# 'speedy-sub-model4': SIZE=128 and CNN=[32,32,64,128,2] and dropout=0.5
# 'speedy-sub-model5': SIZE=128 and CNN=[32,32,64,128,2] and AUGMENTS={:}, dropout=0.5, more stats
# ...
# 8: batch normalization, no augments, deeper cnn, but batchnorm was wrong
# 9: same as above, but batch norm was right
# May only predict on most recent model trained
modelname = 'speedy-sub-model13'
modeldir = './models/'+modelname+'/'
predictdir = './predictimages/'
datadir = './data/'

# I should put this print statement in the LoadImages function...
# Too many statements if I want it to print augments only when augment=True
print('\nLoading predictor images... ', end='', flush=True)
data = LoadImages(predictdir, size=128, predict=True)
print('Done.')

imgs, names = data.T

# Reshape from (len(imgs),) to... (len(imgs), SIZE, SIZE, nchannels)
imgs = np.array([i for i in imgs])




# Load possible classes and dict of class labels
classes, nclasses = GetClasses(datadir)

### https://stackoverflow.com/questions/33759623/tensorflow-how-to-save-restore-a-model
with tf.Session() as sess:
  print('\nLoading', modeldir+modelname, 'and latest checkpoint...')

  # Load meta graph and restore weights, biases
  saver = tf.train.import_meta_graph(modeldir+modelname+'.meta')
  # Gets one and only most-recent "checkpoint"
  saver.restore(sess, tf.train.latest_checkpoint(modeldir))
  graph = tf.get_default_graph()

  x = graph.get_tensor_by_name('x:0')
  y_true = graph.get_tensor_by_name('y_true:0')
  y_pred = graph.get_tensor_by_name('y_pred:0')
  y_test = np.zeros((len(imgs), nclasses))
  keep_prob = graph.get_tensor_by_name('keep_prob:0')
  training = graph.get_tensor_by_name('training:0')

  feed_dict_test = {x: imgs, y_true: y_test, keep_prob: 1.0, training: False}

  # Results of prediction(s) in form [prob1, prob2, ...]
  results = sess.run(y_pred, feed_dict = feed_dict_test)




### Function to decide if prediction is correct or incorrect
def Outcome(name, result):
  real = name.split('predictimage')[0].split('207')[0] 
  # ".split('207')[0]" added to test data images with id=207...
  # Those are the ones I copied over for further tests.
  pred = classes[np.argmax(result)]
  if real == pred:
    return 'CORRECT'
  else:
    return 'INCORRECT: '+real




### Print results
def PrintResults(save=False):
  # Padding for filename
  p1 = str(len(max(names, key=len)))
  # Rounding, padding for result. Accounts for "(0.rrrrr, 0.rrrrr)"
  r = 5
  p2 = str(2*(r+3)+2)
  # Padding for result class
  p3 = str(len(max(classes, key=len)))

  # Delete items in file to append new items. Wonky way, I know.
  if save:
    with open(predictdir+modelname+'_results.txt', 'w') as f:
      None

  for name,result in zip(names,results):
    s = '{:'+p1+'}  {:>'+p2+'} --> {:'+p3+'} \t {}'
    S = s.format(name, str(tuple(np.round(result,r))),
                       classes[np.argmax(result)],
                       Outcome(name, result))
    # Save to file
    if save:
      with open(predictdir+modelname+'_results.txt', 'a') as f:
        print(S, file=f)
    # Print regardless
    print(S)



### Save Results to file and print
'''
print()
PrintResults(save=True)
'''





###TODO: Do more statistics, ie. quantitative analysis.
### Add a function that if image prediction is correct (or not?) update model?
# May lead to overfitting if I don't change the predicted images, though?


### Histograms of probabilities per watch
speedy_probs = [i[0] for i in results if i[0] > i[1]]
sub_probs = [i[1] for i in results if i[1] > i[0]]

hrange = (0.5, 1.0)

plt.figure()
plt.subplot(121)
# normed/density=True was not normalizing hist to 1, so made weights
w_speedy = np.ones_like(speedy_probs)/float(len(speedy_probs))
plt.hist(speedy_probs, range=hrange, weights=w_speedy)
plt.xlabel('Probability of speedy')
plt.ylabel('Percent of respective images')

plt.subplot(122)
w_sub = np.ones_like(sub_probs)/float(len(sub_probs))
plt.hist(sub_probs, range=hrange, color='r', weights=w_sub)
plt.xlabel('Probability of sub')
plt.show(block=False)
plt.savefig(predictdir+modelname+'_probhist.png')



### Report
# Generate good lists. This predict.py script is so sloppy. Oh well!.
true = []
predicted = []
speedy = 'speedy'
sub = 'sub'
dict = {0: speedy, 1: sub}
for n,r in zip(names,results):
  if speedy in n:
    true.append(speedy)
    predicted.append(dict[np.argmax(r)])
  elif sub in n:
    true.append(sub)
    predicted.append(dict[np.argmax(r)])    
    
report = classification_report(true, predicted)
print(report)




### Confusion Matrix
# From http://scikit-learn.org/stable/auto_examples/model_selection/plot_confusion_matrix.html#sphx-glr-auto-examples-model-selection-plot-confusion-matrix-py
import itertools
def plot_confusion_matrix(cm,
                          classes=[0,1],
                          normalize=False,
                          title='', #Confusion matrix',
                          cmap=plt.cm.Greys):#Paired):#Greys):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if normalize:
        #plt.title('Normalized Confusion Matrix: '+toydataset.capitalize())
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        #print("Normalized confusion matrix")
    #else:
        #plt.title('Confusion Matrix: '+toydataset.capitalize())
        #print('Confusion matrix, without normalization')
    #print(cm)

    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, format(cm[i, j], fmt),
                 horizontalalignment='center',
                 color='white' if cm[i, j] > thresh else 'black')
                 #color='black' if cm[i, j] > thresh else 'white')

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')


cm = confusion_matrix(true, predicted)#, labels=[speedy,sub])

plt.figure()
plot_confusion_matrix(cm, classes=[speedy,sub], normalize=True)
plt.show(block=False)
plt.savefig(predictdir+modelname+'_cm.png')
